/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.dummygame;

/**
 *
 * @author Acer
 */
public class Axe extends Weapon {

    public Axe(String name, String color) {
        super(name, color, 150);
        System.out.println("Axe created.");

    }

    @Override
    public void printWeapon() {
        super.printWeapon();
        System.out.println(" Axe\n");
    }
}
