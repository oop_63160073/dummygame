/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.dummygame;

/**
 *
 * @author Acer
 */
public class Bow extends Weapon {

    public Bow(String name, String color) {
        super(name, color, 120);
        System.out.println("Bow created.");
    }

    @Override
    public void printWeapon() {
        super.printWeapon();
        System.out.println(" Bow\n");
    }
}
