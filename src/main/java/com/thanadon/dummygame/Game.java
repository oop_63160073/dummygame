/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.dummygame;

/**
 *
 * @author Acer
 */
public class Game {

    private Dummy dummy;

    public Game() {
    }

    public void setDummy(Dummy dummy) {
        this.dummy = dummy;
    }

    public void showDummy() {
        if (dummy.getHP() <= 0) {
            System.out.println("(x_x) ---> Dummy is dead.");
        } else {
            System.out.println("(^_^) ---> Dummy HP: " + dummy.getHP());
        }
    }
    
    
}
