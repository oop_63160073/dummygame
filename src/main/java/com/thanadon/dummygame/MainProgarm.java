/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.dummygame;

import java.util.Scanner;
/**
 *
 * @author Acer
 */
public class MainProgarm {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        
        Axe axe = new Axe("Wolf", "Black");
        axe.printWeapon();

        Sword sword = new Sword("Favonius", "Red");
        sword.printWeapon();

        Bow bow = new Bow("Amos", "White");
        bow.printWeapon();
        
        Dummy dummy = new Dummy(2700);
        
        Game game = new Game();
        game.setDummy(dummy);
        
        axe.setDummy(dummy);
        sword.setDummy(dummy);
        bow.setDummy(dummy);
        
        while (true){
            game.showDummy();
            System.out.println("Chose Weapon:Enter a(Axe), s(Sword), b(Bow), e(End Game)"); //ใส่ a คือขวาน s คือดาบ b คือธนู e คือจบเกม
            String type = kb.next();
            if(type.equals("e")){
                System.out.println("End Game...");
                break;
            }
            
            System.out.println("Number of Hit:Enter Number ");
            int num = kb.nextInt();
            
            if(type.equals("a")){ 
                AxeAttack(num, axe);
            }else if(type.equals("s")){
                SwordAttack(num, sword);
            }else if(type.equals("b")){
                BowAttack(num, bow);
            }   
                
        }
    }

    private static void BowAttack(int num, Bow bow) {
        if(num <=0){
            System.out.println("Number must more than zero!!!!!!!");
        }else if(num > 1){
            bow.attack(num);
        }else{
            bow.attack();
        }
    }

    private static void SwordAttack(int num, Sword sword) {
        if(num <=0){
            System.out.println("Number must more than zero!!!!!!!");
        }else if(num > 1){
            sword.attack(num);
        }else{
            sword.attack();
        }
    }

    private static void AxeAttack(int num, Axe axe) {
        if(num <=0){
            System.out.println("Number must more than zero!!!!!!!");
        }else if(num > 1){
            axe.attack(num);
        }else{
            axe.attack();
        }
    }
}
