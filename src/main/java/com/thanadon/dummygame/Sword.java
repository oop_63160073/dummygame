/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.dummygame;

/**
 *
 * @author Acer
 */
public class Sword extends Weapon {

    public Sword(String name, String color) {
        super(name, color, 100);
        System.out.println("Sword created.");
    }

    @Override
    public void printWeapon() {
        super.printWeapon();
        System.out.println(" Sword\n");
    }
}
