/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.dummygame;

/**
 *
 * @author Acer
 */
public class Weapon {

    protected String name;
    protected String color;
    protected int atk;
    protected Dummy dummy;

    public Weapon(String name, String color,int atk) {
        this.name = name;
        this.color = color;
        this.atk = atk;
        System.out.println("Weapon created.");
                
    }

    public void setDummy(Dummy dummy) {
        this.dummy = dummy;
    }

    public void printWeapon() {
        System.out.print(this.color + " " + this.name);
    }
    
     public void attack(){ //overload โจมตี 1 ครั้ง  
        dummy.setHP(dummy.getHP()-atk);
    }
    
    public void attack(int n){ //overload โจมตี n ครั้ง
        dummy.setHP(dummy.getHP()-(atk*n));
    }
}
